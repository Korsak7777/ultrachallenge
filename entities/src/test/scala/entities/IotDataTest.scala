package entities

import org.junit.jupiter.api.Test

class IotDataTest {
  val JSONString = """{
                     |data: {
                     |deviceId: 11c1310e-c0c2-461b-a4eb-f6bf8da2d23c,
                     |temperature: 12,
                     |location: {
                     |latitude: 52.14691120000001,
                     |longitude: 11.658838699999933
                     |},
                     |time: 1509793231
                     |}
                     |}""".stripMargin

  @Test
  def serializeDeserializeTest(): Unit ={
    val x = IotData.parseIotData(JSONString)
    val s = x.getJsonString
    assert(s.equals(JSONString))
  }
}
