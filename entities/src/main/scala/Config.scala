import java.util.Properties

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer

object Config {
  def getProperties(broker: String, group:String = "MyGroup"): Properties = {
    val props = new Properties()
    props.put(ConsumerConfig.GROUP_ID_CONFIG, group)
    props.put("bootstrap.servers", broker)
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getCanonicalName)
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getCanonicalName)
    props.put("acks", "1")//min count of conformation of receipt
    props.put("retries", "0")//count of send retries
    props
  }
}
