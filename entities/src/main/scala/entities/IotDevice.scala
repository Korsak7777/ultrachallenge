package entities

import java.io.OutputStream
import java.util.UUID
import java.util.concurrent.{ScheduledThreadPoolExecutor, TimeUnit}

case object IotDeviceSimulator{
  def generateData(out: OutputStream, deviceQuantity:Int = 3): Unit ={
    val threadCount = if(Runtime.getRuntime().availableProcessors() > deviceQuantity)
      deviceQuantity
    else
      Runtime.getRuntime().availableProcessors()
    val ex = new ScheduledThreadPoolExecutor(threadCount)

    def task(id: UUID = UUID.randomUUID()) = new Runnable {
      def run() = {
        out.write(IotData.generateData(id).getJsonString.getBytes)
        out.flush()
      }
    }
    for(i <- 0 until threadCount){
      ex.scheduleAtFixedRate(task(), 0, 1, TimeUnit.SECONDS)
    }
  }
}
//
//object Main{
//  def main(args: Array[String]): Unit = {
//    IotDeviceSimulator.generateData(System.out)
//  }
//}
