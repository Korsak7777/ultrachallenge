package entities

import java.util.UUID
import java.util.concurrent.ThreadLocalRandom

import org.json.JSONObject

case class IotData(deviceId: UUID,
                   temperature: Int,
                   latitude: Double,
                   longitude: Double,
                   time: Long){
  def getJsonString: String = {
    s"""{
        |data: {
        |deviceId: $deviceId,
        |temperature: $temperature,
        |location: {
        |latitude: $latitude,
        |longitude: $longitude
        |},
        |time: $time
        |}
        |}""".stripMargin
  }
}

object IotData{
  def generateData(id: UUID): IotData = {
    IotData(id,
      ThreadLocalRandom.current.nextInt(-273, Int.MaxValue), //from absolute null to infinity
      ThreadLocalRandom.current.nextDouble(-90, 90), //from south to north
      ThreadLocalRandom.current.nextDouble(-180, 180), //from west to east to west
      System.currentTimeMillis())
  }

  def parseIotData(jsonString: String): IotData = {
    val obj = new JSONObject(jsonString)
      .getJSONObject("data")
    IotData(
      UUID.fromString(obj.getString("deviceId")),
      obj.getInt("temperature"),
      obj.getJSONObject("location").getDouble("latitude"),
      obj.getJSONObject("location").getDouble("longitude"),
      obj.getLong("time")
    )
  }
}
