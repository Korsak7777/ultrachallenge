import java.util.{Properties, UUID}
import java.util.concurrent.{Callable, ScheduledThreadPoolExecutor, TimeUnit}

//import com.typesafe.scalalogging.Logger
import entities.IotData
import org.apache.kafka.clients.producer.{KafkaProducer, Producer, ProducerRecord}

object Main {
//  val logger = Logger("Generator")

  def main(args: Array[String]): Unit = {
    val topic = args(0)
    val broker = args(1)
    val deviceQuantity = args(2).toInt
    val period = args(3).toInt

    val props = Config.getProperties(broker)

    val procCount = Runtime.getRuntime.availableProcessors
    val threadCount = if( procCount > deviceQuantity) deviceQuantity else procCount
    println(s"we got $threadCount devices")

    val ex = new ScheduledThreadPoolExecutor(threadCount)
    val producer = new KafkaProducer[String, String](props)

    def task(id: UUID = UUID.randomUUID()) = new Runnable {
      def run(): Unit = {
        val record = new ProducerRecord[String, String](topic, id.toString, IotData.generateData(id).getJsonString)
        println(s"send $id at ${System.currentTimeMillis()}")
        producer.send(record)
      }
    }

    try {
      for(_ <- 0 until threadCount){
        ex.scheduleAtFixedRate(task(), 0, period, TimeUnit.SECONDS)
      }
    } finally{
//      ex.shutdown()
//      producer.close()
    }
  }
}
