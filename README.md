# UltraChallenge

Write a simulator for IoT devices. The device will generate data.
The simulator should simulate three different devices and needs to send a signal to Apache Kafka every
second. Implement the simulator as a long running service and create Kafka topic(s) as needed.

###start zookeper
``` sh
cd Downloads/kafka_2.12-2.5.0
bin/zookeeper-server-start.sh config/zookeeper.properties
```
###start kafka
``` sh
cd Downloads/kafka_2.12-2.5.0
bin/kafka-server-start.sh config/server.properties
```
###create topic
``` sh
Downloads/kafka_2.12-2.5.0/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic test_topic
```
####We can now see that topic if we run the list topic command
``` sh
Downloads/kafka_2.12-2.5.0/bin/kafka-topics.sh --list --bootstrap-server localhost:9092
```
### Run the Producer
``` sh
java -cp /home/korsak/IdeaProjects/ultrachallenge/MessageGenerator/target/scala-2.12/messagegenerator_2.12-0.1.0-SNAPSHOT.jar Main test_topic localhost:9092 3 1
```

### Run the Consumer
``` sh
java -cp /home/korsak/IdeaProjects/ultrachallenge/MessageGenerator/target/scala-2.12/messagegenerator_2.12-0.1.0-SNAPSHOT.jar Main test_topic localhost:9092 1
```

