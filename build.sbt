name := "UltraChallenge2"

scalaVersion := "2.13.2"

lazy val global = project
  .in(file("."))
  .aggregate(
    entities,
    MessageGenerator,
    MessageConsumer
  )

lazy val entities = project
  .settings(
    name := "entities",
    settings,
    libraryDependencies ++= commonDependencies
  )

lazy val MessageGenerator = project
  .settings(
    name := "CatSayMeow",
    settings,
    libraryDependencies ++= commonDependencies
  ).dependsOn(
    entities
  )

lazy val MessageConsumer = project
  .settings(
    name := "MessageConsumer",
    settings,
    libraryDependencies ++= commonDependencies ++ Seq(
      dependencies.sparkCore,
      dependencies.sparkStream,
      dependencies.sparkStreamKafka
    )
  )
  .dependsOn(
    entities
  )

lazy val dependencies =
  new {
    val jacksonModule = "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.11.0"
    val json = "org.json" % "json" % "20190722"
    val jupiter = "org.junit.jupiter" % "junit-jupiter" % "5.4.2" % Test
    val jacksonCore = "com.fasterxml.jackson.core" % "jackson-core" % "2.11.0"
    val kafka = "org.apache.kafka" %% "kafka" % "2.5.0"
    val scalaLogging   = "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
    val slf4jApi = "org.slf4j" % "slf4j-api" % "1.7.30"
    val slf4jSimple = "org.slf4j" % "slf4j-simple" % "1.7.30" % Test

    val sparkCore = "org.apache.spark" %% "spark-core" % "2.4.5"
    val sparkStream = "org.apache.spark" %% "spark-streaming" % "2.4.5"
    val sparkStreamKafka = "org.apache.spark" %% "spark-streaming-kafka-0-10" % "2.4.5"
  }

lazy val commonDependencies = Seq(
  dependencies.jacksonCore,
  dependencies.jacksonModule,
  dependencies.json,
  dependencies.jupiter,
  dependencies.kafka,
  dependencies.scalaLogging,
  dependencies.slf4jApi,
  dependencies.slf4jSimple
)


lazy val settings =
  commonSettings

lazy val commonSettings = Seq(
  resolvers ++= Seq(
    "Local Maven Repository" at "file://" + Path.userHome.absolutePath + "/.m2/repository",
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  )
)
