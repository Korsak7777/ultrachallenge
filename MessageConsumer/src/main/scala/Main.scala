import java.util.Properties

import org.apache.log4j.{Level, Logger}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}

object Main {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)

    val topic = args(0)
    val broker = args(1)
    val period = args(2).toInt

    val sc = new SparkContext(new SparkConf().setAppName("Consumer").setMaster("local[2]"))
    val ssc = new StreamingContext(sc, Seconds(period))

    val props: Properties = Config.getProperties(broker)

    val kafkaStream = KafkaUtils.createDirectStream(
        ssc,
        LocationStrategies.PreferConsistent,
        ConsumerStrategies.Subscribe[String, String](
          java.util.Arrays.asList(topic),
          props.asInstanceOf[java.util.Map[String, Object]]
        )
      )

    kafkaStream.foreachRDD(r => {
      println("got an RDD, size = " + r.count())
      r.foreach(s => println(s))
    })

    ssc.start()

    try {
      ssc.awaitTermination()
      println("streaming terminated")
    } catch {
      case e: Exception =>
        println("streaming exception caught in monitor thread")
    }

    // stop Spark
    sc.stop()
  }
}
